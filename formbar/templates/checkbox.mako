<%
readonly = (field.readonly and "disabled") or ""
## TODO: field should return a field specific value. So we do not need to
## handle the special case here if there is no value set. (ti) <2015-07-07
## 14:50> 
selected = field.get_value() or []
%>

% if field.readonly and field.renderer.render_diff == "true":
  <div class="readonlyfield" name="${field.name}" value="${field.get_value()}">
    % if field.get_previous_value() is not None:
      ${renderer._render_diff(_(field.get_previous_value("", expand=True)), _(field.get_value("", expand=True))) or "&nbsp;"}
    % else:
      ${_(field.get_value("", expand=True)) or "&nbsp;"}
    % endif
  </div>
% else:
% for num, option in enumerate(options):
  ## Depending if the options has passed the configured filter the
  ## option will be visible or hidden
  % if option[2]:
    % if len(option) > 3 and option[3].get("caption"):
      <label class="checkbox-header">
        <strong>${_(option[3].get("caption"))}</strong>
      </label><br/>
      % if not (option[1] is None or option[1] == ""):
       <% raise ValueError("option with caption should not have value") %>
      % endif
    % else:
      <label class="checkbox-inline">
      % if option[1] in selected:
          <input type="checkbox" id="${field.id}-${num}" name="${field.name}" value="${option[1]}" checked="checked" ${readonly} datatype="${get_field_type(field)}"/>
          ## Render a hidden field for selected readonly values to make sure the
          ## value is actually submitted.
          % if readonly:
            <input type="hidden" id="${field.id}-${num}" name="${field.name}" value="${option[1]} datatype="${get_field_type(field)}""/>
          % endif
      % else:
          <input type="checkbox" id="${field.id}-${num}" name="${field.name}" value="${option[1]}" ${readonly} datatype="${get_field_type(field)}"/>
      % endif
        ${_(option[0])}
        % if option[3].get("info"):
        <span class="formbar-tooltip glyphicon glyphicon-info-sign hidden-print" data-toggle="tooltip" data-html="true" data-original-title="${option[3].get('info')}"></span>
        % endif
      </label>
      % if field.renderer.align == "vertical" and num < len(options):
        <br/>
      % endif
    % endif
      ## Prevent loosing already set values. In case a already selected value is
      ## filtered for some reason than render a hidden input element with the
      ## value except the user explicit sets the "remove_filtered" config var.
  % elif not field.renderer.remove_filtered == "true" and option[1] in selected:
        <input type="hidden" id="${field.id}" name="${field.name}" value="${option[1]} datatype="${get_field_type(field)}""/>
  % endif
% endfor
## Hack! Empyt value to allow deselecting all options.
% if not field.readonly:
  <input style="display:none" type="checkbox" name="${field.name}" value="" checked="checked" datatype="${get_field_type(field)}"/>
% endif

% endif # render_diff
