% if field.readonly:
  <div class="readonlyfield" name="${field.name}" style="word-break: break-word;">
    % if field.get_previous_value() is not None:
      ${renderer.nl2br(renderer._render_diff(field.get_previous_value(""), field.get_value("")))}
    % else:
      ${renderer.nl2br(field.get_value(""))}
    % endif
  </div>
% else:
  % if field.renderer._config._tree.attrib.get("maxlength_reverse")=="true":
    <textarea data-reverse="${field.renderer._config._tree.attrib.get("maxlength_reverse")}" data-message="${_('characters remaining')}" datatype="${get_field_type(field)}" ${'' if renderer._active else 'readonly=readonly '} class="form-control" maxlength="${field.renderer._config._tree.attrib.get("maxlength")}" id="${field.id}" name="${field.name}" rows="${field.renderer.rows}">${field.get_value()}</textarea>
  % else:
      <textarea data-reverse="" data-message="/ ${field.renderer.maxlength}" datatype="${get_field_type(field)}" ${'' if renderer._active else 'readonly=readonly '} class="form-control" maxlength="${field.renderer._config._tree.attrib.get("maxlength")}" id="${field.id}" name="${field.name}" rows="${field.renderer.rows}">${field.get_value()}</textarea>
  % endif
  % if field.renderer._config._tree.attrib.get("maxlength") and not (field.renderer.maxlength_display == 'false'):
    % if field.renderer._config._tree.attrib.get("maxlength_reverse")=="true":
        <div class="remainingchars pull-right text-muted small">${ int(field.renderer._config._tree.attrib.get("maxlength")) - len(field.get_value())} ${_('characters remaining')}</div>
    % else:
            <div class="remainingchars pull-right text-muted small">${ len(field.get_value()) } / ${field.renderer.maxlength}</div>
    %endif
  % endif
% endif
