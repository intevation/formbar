% if field.readonly:
  <div class="readonlyfield" name="${field.name}">
    % if field.get_previous_value() is not None:
      ${renderer._render_diff(_(field.get_previous_value("")), _(field.get_value("")))}
    % else:
      ${field.get_value("")}
    % endif
  </div>
  <input class="form-control ${get_field_type(field)}" type="hidden" datatype="${get_field_type(field)}" id="${field.id}" name="${field.name}" value="${field.get_value()}"/>
% else:
  %if get_field_type(field) in ["float","integer"]:
    <input data-reverse="${str(field.renderer.maxlength_reverse)}" data-message="/ ${field.renderer.maxlength}" ${'' if renderer._active else 'readonly=readonly '} class="form-control ${get_field_type(field)}" type="number" step="${field.renderer.step or 'any'}" datatype="${get_field_type(field)}" id="${field.id}" name="${field.name}" value="${field.get_value()}" maxlength="${field.renderer.maxlength}" min="${field.renderer.min or ''}" max="${field.renderer.max or ''}" ${field.autofocus and 'autofocus'}/>
  %else:
    <input data-reverse="${str(field.renderer.maxlength_reverse)}" data-message="/ ${field.renderer.maxlength}" ${'' if renderer._active else 'readonly=readonly '} class="form-control ${get_field_type(field)}" type="text" datatype="${get_field_type(field)}" id="${field.id}" name="${field.name}" value="${field.get_value()}" maxlength="${field.renderer.maxlength}" ${field.autofocus and 'autofocus'}/>
  %endif
  % if field.renderer.maxlength_display=="true" and field.renderer.maxlength:
    %if field.renderer.maxlength_reverse=="true":
        <div id="${field.id}_feedback" class="remainingchars pull-right text-muted small"> ${ int(field.renderer.maxlength) - len(field.get_value()) } / ${field.renderer.maxlength}</div>
    %else:
        <div id="${field.id}_feedback" class="remainingchars pull-right text-muted small"> ${ len(field.get_value()) } / ${field.renderer.maxlength}</div>
    %endif
  % endif
% endif
