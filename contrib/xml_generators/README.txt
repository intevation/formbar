Needs pyyaml.
(spec.yaml should be utf-8 encoded with unix linebreaks.)

Usage:
   python source_generator.py <spec>.yaml

   Generates the source part of the spec from the yaml.

Usage:
   python layout_generator.py <spec>.yaml

   Generates a snippet part of the spec from the yaml.


